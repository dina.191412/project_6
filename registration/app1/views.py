from django.shortcuts import render,HttpResponse,redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url="login")

def Homepage(request):
  return render(request,"homepage.html")

def SignupPage(request):
  if request.method=="POST":
    uname=request.POST.get("uname")
    email=request.POST.get("e-mail")
    pass1=request.POST.get("password")

    my_user=User.objects.create_user(uname,email,pass1)
    my_user.save()
    return redirect("login")
    
  return render(request,"signup.html")

def LoginPage(request):
  if request.method=="POST":
    username=request.POST.get("uname")
    pass1=request.POST.get("password")
    user=authenticate(request,username=username,password=pass1)
    
    if user is not None:
      login(request,user)
      return redirect("home")
    else:
      return HttpResponse("Username or password is incorrect!!!") 
  return render(request,"login.html")


def LogoutPage(request):
  logout(request)
  return redirect("login")


def render_python_file(request):
  
  return render(request,"homepage.html")    
    

