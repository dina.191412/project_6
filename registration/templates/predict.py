
import numpy as np
import pickle
import streamlit as st
loaded_model=pickle.load(open('C:/xampp/htdocs/PROJECT6/registration/templates/trained_model.sav','rb'))

#creating a function for prediction
def heart_disease_prediction(input_data):

    input_data_as_numpy_array=np.asarray(input_data)
    input_data_reshaped=input_data_as_numpy_array.reshape(1,-1)
    prediction=loaded_model.predict(input_data_reshaped)
    print(prediction)
    if(prediction[0]==0):
        return'The person does not have heart disease'
    else:
        return'The person has heart disease'
        
def main():
    st.title('Heart Disease Prediction')
    
    
    age=st.text_input('Your age')
    sex=st.text_input('Enter gender:Value 0:,Value 1:Male,Value 0:Female')
    cp=st.text_input('Enter chest pain type:Value 0=typical angina,Value 1=atypical angina,Value2=non_typical pain,Value3=assymptomatic')
    restbps=st.text_input('Your blood pressure in mmHg')
    chol=st.text_input('Enter cholesterol level in mg/dl')
    fbps=st.text_input('Enter 1 if cholesterol>120mg/dl and 0 if not')
    restecg=st.text_input('Enter restecg:Value 0=normal, Value 1=having ST-T abnormalities, Value 2=showing probalble or definite left ventricular hypertrophy')
    thalach=st.text_input('Enter maximum heart rate achieved')
    exang=st.text_input('Enter exercise indused angina:Value 0=no, Value 1=yes')
    oldpeak=st.text_input('Enter ST depressio indused by exercise relative to rest')
    slope=st.text_input('Enter the slope of the peak exercise ST segment:Value 0=unslopping,Value 1=flat,Value 2=downslopping')
    ca=st.text_input('Enter number of major blood vessels')
    thal=st.text_input('Enter Value 0:normal, Value 1:fixed defect,Value2:reversable defect')
 
    
    diagnosis=''
    
    #creating a button for prediction
    if st.button('Heart Disease Test Result'):
        diagnosis = heart_disease_prediction([
         int(age),
         int(sex),
         int(cp),
         int(restbps),
         int(chol),
         int(fbps),
         int(restecg),
         int(thalach),
         int(exang),
         int(oldpeak),
         int(slope),
         int(ca),
         int(thal)
         ])

    st.success(diagnosis)


if __name__=='__main__':
    main()  